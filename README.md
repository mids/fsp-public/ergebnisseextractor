# Stud. Wahlergebnisse Extractor

Extrahiert die Wahlergebnisse der studentischen Wahlen als .json von der .pdf.

## Abhängigkeiten

```bash
sudo apt install poppler-utils python3 jq
```

## Usage

```
Usage: ./extract.sh <studentische wahlergebnisse .pdf>
  Extrahiert die Wahlergebnisse der Wahlen zum Studierendenparlament als .json von der .pdf.
```

### Beispiel

```bash
$ wget https://www.uni-goettingen.de/de/document/download/cc984fa96e1d7185ad43d0158ee3322f.pdf/Ergebnisse_studOrg_22_23.pdf
$ ./extract.sh Ergebnisse_studOrg_22_23.pdf
[...]
================================================================================
       Bitte Listendaten überprüfen:
  +--------------------------------+----------------+-----------------+
  | Liste                          | Anzahl Stimmen | Anzahl Personen |
  +--------------------------------+----------------+-----------------+
  | GDF (ehem. ADF) - Gemeinschft  |           2731 |             149 |
  | Juso-Hochschulgruppe           |            743 |              44 |
  | Grüne Hochschulgruppe - GHG    |           2578 |             139 |
  | ALL (Alternative Linke Liste)  |            932 |              61 |
  | Schwarz-rot Kollabs            |            160 |              24 |
  | Liberale Hochschulgruppe Götti |            419 |              21 |
  | Volt &amp; Die LISTE           |            453 |              26 |
  | Nerdcampus &amp; Grün und Tech |            500 |              16 |
  | RCDS - Ring Christlich -Demokr |            569 |              49 |
  +--------------------------------+----------------+-----------------+
================================================================================
[Di 14. Mär 03:39:54 CET 2023] Done.

Ausgabe in ./output/listen.json
```
