#!/bin/bash

set -e

ROWOFFSET="$1"

PAGEFILE="$2"
cp "$PAGEFILE" ./curpage.xml

rm -rf ./curpage
mkdir -p ./curpage                             
pushd curpage >/dev/null

cat "../curpage.xml" | grep '^<text' | while read line ; do
	row="$(( $ROWOFFSET + $(echo "$line" | tr ' ' '\n' | grep '^top=' | head -n 1 | cut -d '"' -f 2) ))"
	echo "$line" >> "row-${row}.txt"
done

function mergerows() {
	#echo "MEGING ROWS: '$1' <- '$2'"
	#cat "$1" | tr '\n' '|'
	#echo ""
	#cat "$2" | tr '\n' '|'
	#echo "==="
	cat "$2" >> "$1"
	rm -f "$2"
}
ls row-*.txt | sort -V | while read line ; do
	if [ -f "$line" ]; then
		ROWNUM="$(echo "$line" | cut -d '-' -f 2 | cut -d '.' -f 1)"
		#echo "$ROWNUM"
		if                   [ -f "./row-$(( $ROWNUM + 1 )).txt" ]; then
			mergerows "$line" "./row-$(( $ROWNUM + 1 )).txt"
		elif                 [ -f "./row-$(( $ROWNUM + 2 )).txt" ]; then
			mergerows "$line" "./row-$(( $ROWNUM + 2 )).txt"
		elif                 [ -f "./row-$(( $ROWNUM + 3 )).txt" ]; then
			mergerows "$line" "./row-$(( $ROWNUM + 3 )).txt"
		fi
	fi
done

function sortcolumns() {
	#cat "$1"
	#echo "==== ROW: $1" >&2
	cat "$1"  | sed 's,^\(<text [^>]*left="\)\([0-9]*\)\(".*\)$,\2:\1\2\3,' | sort -V | sed 's,^[0-9]*:,,'
	# <text top="333" left="658" width="61" height="11" font="7">Oliver Mark</text>
	#cat "$1" >&2
}

function getkennwoerterrows() {
	(
		grep 'Kennwort:' row-*.txt | sort -V | grep 'font="8"' | sed 's@^row-\([0-9]*\)\.txt:<text.*><b>Kennwort: \(.*\)</b></text>$@{"_row": \1, "kennwort": "\2"}@'
	) | jq -s '.'
}

function getpeople() {
(
	ls row-*.txt | sort -V |  while read row ; do
		numtexts="$(cat "$row" | wc -l)"
		if [ "$numtexts" -ge 4 ] && [ "$numtexts" -le 6 ]; then # Only rows with 4-6 texts
			# Could be a person
			if [ "$(cat "$row" | tr ' ' '\n' | grep '^font=' | cut -d '"' -f 2 | sort -u | grep -v '^7$' | grep -v '^10$' | grep -v '^11$' | wc -l)" = "0" ] ; then # Only fonts are 7, 10 and 11
				if [ "$(cat "$row" | grep "Zahl der" | wc -l)" = "0" ] ; then # Ignore headers
				
					# Should be a Person
					#echo ""
					#echo "$row" >&2
					#echo "${row} : $(cat "$row" | wc -l) $(cat "${row}" | cut -d '>' -f 2- | tr '\n' '|')" >&2
					#cat "$row"
					rowdata="$(sortcolumns "$row" | head -n 4 | cut -d '>' -f 2 | cut -d '<' -f 1 | sed 's,  *, ,g')"
					listenplatz="$(echo "$rowdata" | sed '1q;d')"
					nachname="$(echo "$rowdata"    | sed '2q;d')"
					vorname="$(echo "$rowdata"     | sed '3q;d')"
					stimmen="$(echo "$rowdata"     | sed '4q;d')"
					printf '{"listenplatz": %d, "nachname": "%s", "vorname": "%s", "stimmen": %s, "_row": %d}\n' "$listenplatz" "$nachname" "$vorname" "$stimmen" "$(echo "$row" | cut -d '-' -f 2 | cut -d '.' -f 1)"
				fi
			fi
		fi
	done
) | jq -s '.'
}

(
printf '{'
printf '"kennwoerter": '
getkennwoerterrows
printf ', "people": '
getpeople
printf '}'
) | jq -c '.' > ../curpage.json

popd >/dev/null

rm -f ../curpage.xml

mkdir -p pagesjson
mv ./curpage.json "./pagesjson/page-$(echo "$2" | cut -d '-' -f 2 | cut -d '.' -f 1).json"
