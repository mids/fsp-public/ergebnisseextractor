#!/bin/bash

set -e

if [ "$#" != "1" ]; then
	echo "Usage: $0 <studentische wahlergebnisse .pdf>"
	echo "  Extrahiert die Wahlergebnisse der Wahlen zum Studierendenparlament als .json von der .pdf."
	exit 1
fi

date

rm -rf tmp
mkdir -p tmp

pdftohtml -hidden -xml "$1" ./tmp/data.xml

pushd tmp >/tmp/null

# extract pages
function extractpages() {
	rm -rf pages
	mkdir -p pages
	CURPAGE="./pages/header.xml"
	cat data.xml |  while  read line ; do
		if echo "$line" | grep -q '^<page'  ; then
			CURPAGENUM="$(echo "$line" | tr ' ' '\n' | grep '^number=' | cut -d '"' -f 2)"
			CURPAGE="$(printf './pages/page-%2.2d.xml' "${CURPAGENUM}")"
			echo "New Page[${CURPAGE}]: $line"
			echo "$line" > "${CURPAGE}"
		else
			echo "$line" >> "${CURPAGE}"
		fi
		#echo "$line"
	done
}

function trimspaces() {
	cat - 
}
function convertpages() {
	rm -rf ./pagesjson 
	mkdir -p ./pagesjson 
	ls ./pages/page-*.xml | while read line ; do
		if cat "$line" | grep -q 'Wahlergebnis der Wahlen zum Studierendenparlament' ; then
			ROWOFFSET="$(( 10000 * $(echo "$line" | cut -d '-' -f 2 | cut -d '.' -f 1 | sed 's,^0*,,') ))"
			echo "converting: $line"
			../convertpage.sh "$ROWOFFSET" "$line"
		fi
	done
	find ./pagesjson -type f | sort -V | while read line ; do cat "$line" ; done | jq -s '.' \
		| jq '.' | sed 's,  *, ,g;s,^\(.*"[^"]*": *"\)\([^"]*\) * \(".*\)$,\1\2\3,g;s,^\(.*"[^"]*": *"\) * \([^"]*\)\(".*\)$,\1\2\3,g' \
		| jq '.' > pages.json
}

extractpages
convertpages
#cat pages.json | jq '.'
../extractlisten.py
mkdir -p ../output
cat listen.json | jq '.' > ../output/listen.json

popd >/dev/null

echo "[$(date)] Done."
echo ""
echo "Ausgabe in ./output/listen.json"
