#!/usr/bin/env python3

import json

def getkennwort(kennwoerter, person):
    prow = person["_row"]
    prev = None
    for k in kennwoerter:
        if k["_row"] >= prow:
            if prev is None:
                raise Exception("Found but prev is None.", person, k, kennwoerter)
            return prev["kennwort"]
        prev = k
    #raise Exception("Kennwort für Person nicht gefunden.", person, kennwoerter)
    return prev["kennwort"]


def main():
    f = open("pages.json")
    pages = json.load(f)
    f.close()
    #print(pages)
    people = []
    kennwoerter = []
    for page in pages:
        people += page["people"]
        kennwoerter += page["kennwoerter"]
    #print(people)
    #print(kennwoerter)

    listen = {}
    for liste in kennwoerter:
        listen[liste["kennwort"]] = liste
        liste["people"] = {}
        liste["stimmen"] = 0
    #print(listen)
    
    for person in people:
        kennwort = getkennwort(kennwoerter, person)
        #print(kennwort, person)
        liste = listen[kennwort]
        liste["stimmen"] += person["stimmen"]
        fullname = person["vorname"] + " " + person["nachname"]
        person["fullname"] = fullname
        if fullname in liste["people"]:
            raise Exception("Namenskollision! Liste beinhaltet schon eine Person mit dem Namen.", fullname, person, liste)
        liste["people"][fullname] = person
    #print(listen)

    for listenname, liste in listen.items():
        expectednum = len(liste["people"].keys())
        print("Testing: ", listenname)
        for i in range(expectednum):
            found = False
            for fullname, person in liste["people"].items():
                if person["listenplatz"] == i+1:
                    found = True
                    print("  Found[" + str(i+1) + "]: " + person["fullname"])
            if not found:
                raise Exception("Person mit Listenplatz " + str(i+1) + " nicht gefunden.", listenname, liste)

    #for lname, l in listen.items():
    #    print()
    #    print("="*86)
    #    #print(lname, "({} Sitze)".format(l["sitze"]))
    #    print(lname)
    #    print("="*86)
    #    print()
    #    print("+-{:30.30}-+-{:5.5}-+-{:5.5}-+-----+-----+-----+-----+-----+-----+".format("--------------------------------","------","------"))
    #    print("| {:30.30} |Listen-|Stimmen|             Anwesend              |".format("Name"))
    #    print("| {:30.30} |platz  |       | Von | Bis | Von | Bis | Von | Bis |".format(""))
    #    print("+-{:30.30}-+-{:5.5}-+-{:5.5}-+-----+-----+-----+-----+-----+-----+".format("--------------------------------","------","------"))
    #    #print(l)
    #    plist = []
    #    for pname, p in l["people"].items():
    #        p["fullname"] = pname
    #        plist.append(p)
    #    plist.sort(key=lambda x: x["listenplatz"], reverse=False)
    #    plist.sort(key=lambda x: x["stimmen"], reverse=True)
    #    #print(plist)
    #    for p in plist:
    #        name = p["fullname"]
    #        print("| {:30.30} | {:5} | {:5} |     |     |     |     |     |     |".format(name,p["listenplatz"],p["stimmen"]))
    #    print("+-{:30.30}-+-{:5.5}-+-{:5.5}-+-----+-----+-----+-----+-----+-----+".format("--------------------------------","------","------"))


    for lname, l in listen.items():
        plist = []
        for pname, p in l["people"].items():
            plist.append(p)
        plist.sort(key=lambda x: x["listenplatz"], reverse=False)
        plist.sort(key=lambda x: x["stimmen"], reverse=True)
        l["people_in_order"] = plist

    print("="*80)
    print("       Bitte Listendaten überprüfen:")
    print("  +-{:30.30}-+-{:14.14}-+-{:15.15}-+".format("-"*80, "-"*80, "-"*80))
    print("  | {:30.30} | {:14.14} | {:15.15} |".format("Liste", "Anzahl Stimmen", "Anzahl Personen"))
    print("  +-{:30.30}-+-{:14.14}-+-{:15.15}-+".format("-"*80, "-"*80, "-"*80))
    for listenname, liste in listen.items():
        print("  | {:30.30} | {:14} | {:15} |".format(listenname, liste["stimmen"], len(liste["people"].keys())))
    print("  +-{:30.30}-+-{:14.14}-+-{:15.15}-+".format("-"*80, "-"*80, "-"*80))
    print("="*80)



    fout = open("listen.json", "w")
    json.dump(listen , fout)
    fout.close()


if __name__ == "__main__":
    main()

